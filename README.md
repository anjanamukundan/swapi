#Swapi Data Connector
Swapi Data Connector is a Python package to connect to SWAPI, The Starwars API

##Installation

###Requirements

Swapi data connector requires Python 3.4 or later. This package is not tested with any version of Python 2.7. The
following third party packages are required, which will be auto-installed if you are using pip
* python-dateutil
* requests



### Installing from bitbucket
If you need the latest version, you can install it directly from github repository.

`pip install git+https://anjanamukundan@bitbucket.org/anjanamukundan/swapi.git`


## User Guide

### Creating a Connection to swapi-data.org API

Before retrieving data, you need to create a connection object.

```python
from swapidata.connector import Connector

connection = Connector()
```

###Connector object attributes

*base_url - Gives the base api url
*people_endpoint - Gives the API endpoint to fetch people
*vehicles_endpoint - Gives the API endpoint to fetch vehicles
*starships_endpoint - Gives the API endpoint to fetch starships
*species_endpoint - Gives the API endpoint to fetch species
*films_endpoint - Gives the API endpoint to fetch films
*planets_endpoint - Gives the API endpoint to fetch planets

###Connector Object methods

###get_people()
Returns a **DataSet** object contains **People** objects.

###get_vehicles()
Returns a **DataSet** object contains **Vehicles** objects.

###get_starships()
Returns a **DataSet** object contains **Starships** objects.

###get_species()
Returns a **DataSet** object contains **Species** objects.

###get_films()
Returns a **DataSet** object contains **Films** objects.

###get_planets()
Returns a **DataSet** object contains **Planets** objects.

### DataSet Objects

Methods like *get_people*, *get_ve* etc will return a **DataSet** object. **DataSet** is an iterable object.
You can use it like any other iterable such as **list**, **tuple** etc. Operations like using with a *for* loop,
checking length using *len*, subscripting, slicing, reversing etc are supported


## Caching and Lazy Evaluation

Values in a **DataSet** object are cached during the creation of **DataSet**. Subsequent calls to methods which returns
a **DataSet** will be returning the cached values.

A **DataSet** will not perform any API calls during its creation. There will not be any values in a **DataSet** after
its creation. API call is executed only when an action which uses the data is executed such as using in a for loop,
checking the length of **DataSet** etc.