from .datasets import Dataset
from .swapi import Planets, People, Starships, Vehicles, Species, Films


class Connector:
    def __init__(self):

        self.base_url = "https://swapi.co/api/"
        self.people_endpoint = "{base_url}people/".format(base_url=self.base_url)
        self.starships_endpoint = "{base_url}starships".format(base_url=self.base_url)
        self.planets_endpoint = "{base_url}planets".format(base_url=self.base_url)
        self.films_endpoint = "{base_url}films".format(base_url=self.base_url)
        self.vehicles_endpoint = "{base_url}vehicles".format(base_url=self.base_url)
        self.species_endpoint = "{base_url}species".format(base_url=self.base_url)
        self.planets_endpoint = "{base_url}planets".format(base_url=self.base_url)

        self.__people_list = []
        self.__vehicles_list = []
        self.__planets_list = []
        self.__species_list = []
        self.__films_list = []
        self.__starships_list = []

    def get_people(self):

        self.__people_list = Dataset(People, self.people_endpoint)
        return self.__people_list

    def get_vehicles(self):

        self.__vehicles_list = Dataset(Vehicles, self.vehicles_endpoint)
        return self.__vehicles_list

    def get_planets(self):

        self.__planets_list = Dataset(Planets, self.planets_endpoint)
        return self.__planets_list

    def get_species(self):

        self.__species_list = Dataset(Species, self.species_endpoint)
        return self.__species_list

    def get_films(self):

        self.__films_list = Dataset(Films, self.films_endpoint)
        return self.__films_list

    def get_starships(self):

        self.__starships_list = Dataset(Starships, self.starships_endpoint)
        return self.__starships_list