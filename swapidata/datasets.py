from .utils import fetch_data_from_api


class Dataset:

    def __init__(self, klass, end_point, data_list=None):

        self._klass = klass

        self._new_set = []
        self._data_list = data_list
        self._end_point = end_point
        # self.value = value

    def _create_data_set_item(self, _data):
        """create data set"""

        data_set_item = self._klass(**_data)

        return data_set_item

    def _load_data_set(self):
        """Loads data """
        if not self._data_list:
            self._data_list = (fetch_data_from_api(self._end_point))

        self._data_set = list(map(self._create_data_set_item, self._data_list))

    def __iter__(self):
        self._load_data_set()

        for data in self._data_set:
            yield data

    def __len__(self):
        self._load_data_set()
        return len(self._data_list)

