import requests


def fetch_data_from_api(end_point):

    data_list = []
    response = requests.get(end_point)
    if response.status_code == 200:
        data_list.extend(response.json()['results'])
        next_page = response.json()['next']
        while next_page:
            next = requests.get(next_page)
            data_list.extend(next.json()['results'])
            next_page = next.json()['next']
        return data_list
    return[]