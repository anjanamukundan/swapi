class Starwars:
    def __init__(self, **dic):
        for key in dic:
            setattr(self, key, dic[key])

    def __repr__(self):
        return self.name


class People(Starwars):
    pass


class Vehicles(Starwars):
    pass


class Starships(Starwars):
    pass


class Planets(Starwars):
    pass


class Species(Starwars):
    pass


class Films(Starwars):
    pass
